JSON configuration file management module. Supports:
- booleans, numbers, arrays, strings, objects
- dot notation
- setters that create parent objects
- getters that return undefined if path does not exist
- default config values if no value present
- subconfig for sharding on config data

Usage
---

```js
const config = await require('seamless-config').load()
config.default('port', 80)
config.default('author', {name: 'me', email: 'abc@xyz.com'})

config.get('port') // 80
config.get('author.name') //me

config.set('app.title', 'my app')
config.set('author.email', 'newThing')
config.get('app.title') //my app
config.get('author.email') //newThing

config.remove('app')
config.get('app.title') //undefined
```

API
---

`[param]` indicates param is optional.

Some of these are promises that will return on file save/read, and could throw errors.

#### Config
All methods that don't return something (ie, get) are chainable/return config instance
```js
config.load([file]) //promise
//file is config.json by default

config.save() //promise

config.clear([save]) //promise
//erases all config data. If defaults are configured, will populate defaults
//save is true by default, will save to file on every set

config.set(key, value, [save]) //promise
//save is true by default, will save to file on every set

config.get([key])
//if no key is provided, will return all config data

config.remove(key, [save]) //promise
//save is true by default, will save to file on every set

config.default(key, value, [save]) //promise
//if key's value is not already set or in file, will set to this value & save to file
//save is true by default, will save to file on every set

config.clearDefaults([save])
//will remove all default directives (but any values changed by defaults will remain changed)
//save is true by default, will save to file on every set


config.file
//the file config loads/saves from.

config.defaultSave
//determines if config.set() will save without save parameter. Is true by default.


config.Config
//if you need multiple config instances, do new config.Config([file], [defaultSave])

config.getSub(key)
//returns Subconfig for data at the key
```

#### Subconfig
All methods that don't return something (ie, get) are chainable/return the same subconfig instance

If key of `undefined` is given, it works from base of the subconfig.
```js
config.set(key, value, [save]) //promise
//save is true by default, will save to file on every set

config.get([key])
//if no key is provided, will return all subconfig data

config.remove(key, [save]) //promise
//save is true by default, will save to file on every set

config.default(key, value, [save]) //promise
//if key's value is not already set or in file, will set to this value & save to file
//save is true by default, will save to file on every set

config.getSub(key)
//returns Subconfig for data at the key
```
